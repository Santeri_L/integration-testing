const expect = require("chai").expect;
const request = require("request");
const { response } = require("../src/server");
const app = require("../src/server");
const PORT = 3000;

describe("Color Code Converter API", () => {
    before("Starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to Hex conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(body).to.equal("#ff0000");
                done();
            });
        });
    });
    describe("Hex to RBG conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", (done) => {
            const url = baseurl + "/hex-to-rbg?hex=00ff00";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in rgb", (done) => {
            const url = baseurl + "/hex-to-rbg?hex=00ff00";
            request(url, (error, response, body) => {
                expect(body).to.equal("0,255,0");
                done();
            });
        });
    });
    after("Stop server", (done) => {
        server.close();
        done();
    });
});