// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversions", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // # ff0000
            const blueHex = converter.rgbToHex(0, 0, 255); // # 0000ff
            const greenHex = converter.rgbToHex(0, 255, 0); // # 00ff00

            expect(redHex).to.equal("#ff0000"); // red hex value
            expect(blueHex).to.equal("#0000ff"); // blue hex value
            expect(greenHex).to.equal("#00ff00"); // green hex value
        });
    });
    describe("Hex to RGB conversions", () => {
        it("converting hex to rgb", () => {
            const greenHex = converter.hexToRbg("00ff00") // green hex value

            expect(greenHex).to.equal("0,255,0") // green rbg value
        });
    });
});